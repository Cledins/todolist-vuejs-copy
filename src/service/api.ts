import axios from 'axios';
import { resolveComponent } from 'vue';
import { Todo, TodoStatus } from '../model/todo';

const base_url = "http://18.203.223.146:5000/api"

export function create(todo: Todo) {
  axios.post(`${base_url}/todo`, {
    id: todo.id,
    content: todo.content,
    completed: todo.status,
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}

export async function get(): Promise<Todo[]> {
  try {
    return new Promise((resolve, reject) => {
      axios.get<[{id: string, content: string, completed: boolean}]>(`${base_url}/todo`)
        .then(function (response) {
          resolve(toTodoList(response.data));
        })
        .catch(function (error) {
          reject(error);
        })
      }
    );
  }
  catch (err) {
    return [];
  }
}

function toTodoList(data: [{id: string, content: string, completed: boolean}]): Todo[] {
  const todoList = [];
  for (let todoApi of data) {
    const status = todoApi.completed ? TodoStatus.DONE : TodoStatus.TODO;
    const todo = new Todo(todoApi.id, todoApi.content, status);
    todoList.push(todo);
  }
  return todoList;
}